/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hilo;

import Principal.Consulta;

public class Consumo extends Thread{

 private int min;
 private int max;

    public Consumo(int min, int max) {
        this.min = min;
        this.max = max;
    }
   
    @Override
    public void run() {
      llamado();
    }
    
    
    public void llamado(){
        Consulta c = new Consulta();
        c.llenar(getMin(),getMax());
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

}
