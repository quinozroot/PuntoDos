/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pool;

import Objeto.Usuario;
import java.util.ArrayList;

/**
 *
 * @author gohan
 */
public class PoolUser {

    ArrayList<Usuario> listU = new ArrayList<Usuario>();

    private static PoolUser poolUser;

    
    public static synchronized PoolUser getInstance() {
        if (poolUser == null) {
            poolUser = new PoolUser();
        }
        return poolUser;
    }

    
    public void addUsuario(String nom, String edad) {
        Usuario u = new Usuario(nom, edad);
        listU.add(u);
    }
    
    public ArrayList<Usuario> getList(){
      return  listU; 
    }
}
